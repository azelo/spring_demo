package phoenix.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Hello {

    public static void main(String args[]) {
        
        ApplicationContext context = new ClassPathXmlApplicationContext("beanconfig.xml");

        Greeter greeter = (Greeter) context.getBean("greeter");
        
        greeter.greet();
    }

}

